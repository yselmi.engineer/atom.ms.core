using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Http;

using Models;
using Models.Payload;
using Models.Scrap;

namespace Services;

public static class JobiScrapper
{

    private static String jobiJobsURL = "https://tungolia.jobi.tn/filter/joboffer";
    private static String jobiSponsoredURL = "https://jobi.tn/api/sponsored/companies";
    static readonly HttpClient client = new HttpClient();

    public static IEnumerable<Company> JobiSponsoredCompanies { get; set; } = new List<Company>();

    public static List<Job> JobiJobs { get; set; } = new List<Job>();

    private static JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
    };


    public static async Task<(Company[], Job[])> Scrap()
    {
        try
        {
            return
            (
                await ScrapCompanies(),
                await ScrapJobs()
            );
        }

        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
            return (new Company[0], new Job[0]);
        }
    }



    private static async Task<Company[]> ScrapCompanies()
    {

        System.Console.WriteLine("calling api");
        HttpResponseMessage response = await client.GetAsync(jobiSponsoredURL);
        response.EnsureSuccessStatusCode();
        string responseBody = await response.Content.ReadAsStringAsync();
        IEnumerable<JobiSponsoredCompanyDTO> jobiSponsoredCompanyDTOList = JsonSerializer.Deserialize<JobiSponsoredCompanyDTO[]>(responseBody, jsonSerializerOptions);

        return jobiSponsoredCompanyDTOList.Select(x => x.Transform()).ToArray();
    }

    private static async Task<Job[]> ScrapJobs()
    {


        System.Console.WriteLine("Scrapping from Jobi...");

        bool finish = false;

        int max = 15;

        int count = 1;

        JobiScrapPayload payload = new JobiScrapPayload();

        while (!finish)
        {
            HttpContent httpContent = new StringContent(
               JsonSerializer.Serialize(payload, jsonSerializerOptions),
               System.Text.Encoding.UTF8,
               "application/json"
               );

            HttpResponseMessage response = await client.PostAsync(jobiJobsURL, httpContent);

            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();

            JobiJobsDtO? jobiJobsDTO = JsonSerializer.Deserialize<JobiJobsDtO>(responseBody, jsonSerializerOptions = new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
            });


            if (jobiJobsDTO?.Hits?.Length == 0 || count > max)
            {
                break;
            }

            JobiJobs?.AddRange(jobiJobsDTO?.Hits.Select(x => x.Transform()));

            count++;
            payload.SetNextPagination();


            System.Console.WriteLine("scrapped from jobi {0} pagination: {1}, next pagination: {2}", jobiJobsDTO?.Hits?.Count(), count - 1, payload.Page);

            System.Threading.Thread.Sleep(3000);
        }



        System.Console.WriteLine("Total jobi jobs found: " + JobiJobs.Count);


        return JobiJobs.ToArray();






        // FBPublisher.PublishMessage(RenderService.GetRenderedJobsAsText(JobiJobs.ToArray()));

    }



}