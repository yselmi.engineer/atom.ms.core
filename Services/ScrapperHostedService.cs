using System;
using System.Timers;
using Microsoft.Extensions.Hosting;
using System.Text.Json;
using System.Net.Http;

using Models;
using Models.Scrap;


namespace Services;

public class ScrapperHostedService : IScrapper, IHostedService
{

    CompanyService companyService { get; set; }
    JobService jobService { get; set; }
    HistoryService historyService { get; set; }
    HostedServiceManager hostedServiceManager { get; set; }
    System.Threading.Timer timer;


    public ScrapperHostedService(CompanyService companyService, JobService jobService, HistoryService historyService, HostedServiceManager hostedServiceManager)
    {
        this.companyService = companyService;
        this.jobService = jobService;
        this.historyService = historyService;
        this.hostedServiceManager = hostedServiceManager;
    }


    public Task StartAsync(CancellationToken cancellationToken)
    {
        Scrap();

        return Task.CompletedTask;

    }

    public Task StopAsync(CancellationToken cancellationToken)
    {

        timer?.Change(Timeout.Infinite, 0);

        return Task.CompletedTask;
    }


    public async Task Scrap()
    {

        try
        {

            hostedServiceManager.PublishingBlocked = true;

            var nextRunTime = DateTime.Today.AddDays(1).AddHours(8);
            var curTime = DateTime.Now;
            var firstInterval = nextRunTime.Subtract(curTime);

            timer = new System.Threading.Timer(
                _ => ScrapJobs(),
                null,
                (int)TimeSpan.Zero.TotalMilliseconds,
                (int)TimeSpan.FromHours(24).TotalMilliseconds
            );
        }

        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

    }


    private async Task ScrapJobs()
    {

        bool hasScrappedTodayAtLeast = await historyService.HasScrappedTodayAtLeast();

         if (hasScrappedTodayAtLeast)
         {
             hostedServiceManager.PublishingBlocked = false;
             return;
         }

        (Company[] companies, Job[] jobs) = await JobiScrapper.Scrap();

        Console.WriteLine("Total jobs found: {0}, sponsored companies", jobs.Length, companies.Length);
        Console.WriteLine("Updating companies Database...");

        await companyService.DeleteManyAsync();
        await companyService.InsertManyAsync(
            companies.Select(x => x.SetHasUsedSponsoring(true)).ToArray(),
            true
        );

        Console.WriteLine("Updating jobs Database...");
        await jobService.DeleteManyAsync();
        await jobService.InsertManyAsync(jobs, true);

        hostedServiceManager.PublishingBlocked = false;

        long count = await jobService.CountAsync();

        Console.WriteLine("Done ! total jobs: {0} ", count);

        DateTime now = DateTime.Now;

        Console.WriteLine("saving history at {0} ", now.ToString("MMMM dd, yyyy"));

        await historyService.InsertOneAsync(
            new History()
            {
                CreatedAt = now,
                Type = HistoryTypes.JobsScrap
            }
        );
    }

}