
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using SmartFormat;
using System.IO;
using Models;

using Microsoft.Extensions.Options;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Http;

namespace Services;




public class RenderService
{

    private static String jobsFilePath = "Files/Jobs/JobsHeader.txt";
    private static String jobContentFilePath = "Files/Jobs/JobContent.txt";


    private static String lambdaNestGenerateJobImageURL = "http://lambda.nest:3000/api/generate/job/image";




    public static String RenderJobsHelloMessage()
    {

        return "Hello,\nHere is today's coolest job offers.\n";

    }
    public static String RenderJobAsText(Job job)
    {

        String content = "";

        ;
        Dictionary<String, String> jobContent = job.RenderAsText();

        if (jobContent.ContainsKey("Title"))
        {
            content = $"{content} **{jobContent.GetValueOrDefault("Title")}** \n";
        }

        if (jobContent.ContainsKey("Company"))
        {
            content = $"{content}Company --> {jobContent.GetValueOrDefault("Company")}\n\n";
        }

        if (jobContent.ContainsKey("Status"))
        {
            content = $"{content}{jobContent.GetValueOrDefault("Status")}\n\n";
        }

        if (jobContent.ContainsKey("MinSalary"))
        {
            content = $"{content}Minium Salary {jobContent.GetValueOrDefault("MinSalary")}DT\n";
        }

        if (jobContent.ContainsKey("MaxSalary"))
        {
            content = $"{content}Maxium Salary {jobContent.GetValueOrDefault("MaxSalary")}DT\n\n";
        }

        if (jobContent.ContainsKey("Description"))
        {
            content = $"{content}{jobContent.GetValueOrDefault("Description")}\n\n";
        }
        if (jobContent.ContainsKey("Benefits"))
        {
            content = $"{content}Benefits:\n{jobContent.GetValueOrDefault("Benefits")}\n\n";
        }
        if (jobContent.ContainsKey("PublishedAt"))
        {
            content = $"{content}Posted online at: {jobContent.GetValueOrDefault("PublishedAt")}\n";
        }
        if (jobContent.ContainsKey("ExpiresAt"))
        {
            content = $"{content}Expires at: {jobContent.GetValueOrDefault("ExpiresAt")}\n\n";
        }

        if (jobContent.ContainsKey("Tags"))
        {
            content = $"{content}{jobContent.GetValueOrDefault("Tags")}";
        }

        return content;

    }

    public static async Task<byte[][]> GetRenderedJobsAsImages(Job[] jobs)
    {

        System.Console.WriteLine("rendering images from lambda nest...");

        HttpClient client = new HttpClient();

        JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
        };


        List<byte[]> imagesBytes = new List<byte[]>();

        foreach (Job job in jobs)
        {

            var payload = new
            {
                Title = job.Title,
                Company = job.Company.Name,
                Logo = job.Company.Logo,
                SalaryMin = job.SalaryMin,
                SalaryMax = job.SalaryMax,
                CreatedAt = job.CreatedAt
            };

            HttpContent httpContent = new StringContent(
               JsonSerializer.Serialize(payload, jsonSerializerOptions),
                System.Text.Encoding.UTF8,
                "application/json"
                );

            HttpResponseMessage response = await client.PostAsync(lambdaNestGenerateJobImageURL, httpContent);
            response.EnsureSuccessStatusCode();

            byte[] responseBody = await response.Content.ReadAsByteArrayAsync();

            imagesBytes.Add(responseBody);

        }

        return imagesBytes.ToArray();

    }

}