using Microsoft.Extensions.Options;
using MongoDB.Driver;



using Models;

namespace Services;




public class JobService
{

    private readonly IMongoCollection<Job> jobsCollection;
    private readonly IMongoCollection<Backup<Job>> jobsBackupCollection;

    public DBService dbService { get; set; }

    public JobService(DBService dbService)
    {
        this.dbService = dbService;
        this.jobsCollection = dbService.GetDB().GetCollection<Job>("Job");
        this.jobsBackupCollection = dbService.GetDB().GetCollection<Backup<Job>>("JobBackup");
    }


    public async Task<List<Job>> GetHottestAsync(int limit = 10)
    {
        var filter = Builders<Job>.Filter;
        return await jobsCollection
         .Find(filter.Empty)
         .SortByDescending(it => it.CreatedAt)
         .SortByDescending(it => it.SalaryMin)
         .Skip(0)
         .Limit(limit)
         .ToListAsync();
    }




    public async Task InsertManyAsync(Job[] jobs, bool backup = false)
    {

        await jobsCollection.InsertManyAsync(jobs);

        if (backup)
        {
            DateTime createdAt = DateTime.Now;

            await jobsBackupCollection.InsertManyAsync(jobs.Select(job => new Backup<Job>()
            {
                CreatedAt = createdAt,
                Data = job
            }).ToArray());
        }
    }

    public async Task DeleteManyAsync() =>
        await jobsCollection.DeleteManyAsync(_ => true);


    public async Task<List<Job>> GetAsync() =>
       await jobsCollection.Find(_ => true).ToListAsync();


    public async Task<long> CountAsync() =>
        await jobsCollection.CountDocumentsAsync(_ => true);

    public async Task<Job> GetAsync(string id) =>
        await jobsCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task InsertOneAsync(Job newJob) =>
        await jobsCollection.InsertOneAsync(newJob);


    public async Task UpdateAsync(string id, Job updatedJob) =>
        await jobsCollection.ReplaceOneAsync(x => x.Id == id, updatedJob);

    public async Task RemoveAsync(string id) =>
        await jobsCollection.DeleteOneAsync(x => x.Id == id);


}