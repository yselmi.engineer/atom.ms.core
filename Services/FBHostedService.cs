using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Timers;
using Microsoft.Extensions.Hosting;
using System.Text.Json;
using System.Net.Http;

using Models;
using Models.FB;


namespace Services;

public class FBHostedService : IHostedService
{


    FBPublisher fbPublisher { get; set; }
    CompanyService companyService { get; set; }
    JobService jobService { get; set; }
    HistoryService historyService { get; set; }
    HostedServiceManager hostedServiceManager { get; set; }

    System.Threading.Timer timer;


    public FBHostedService(FBPublisher fbPublisher, CompanyService companyService, JobService jobService, HistoryService historyService, HostedServiceManager hostedServiceManager)
    {
        this.fbPublisher = fbPublisher;
        this.jobService = jobService;
        this.historyService = historyService;
        this.hostedServiceManager = hostedServiceManager;
    }


    public Task StartAsync(CancellationToken cancellationToken)
    {
        Publish();

        return Task.CompletedTask;

    }

    public Task StopAsync(CancellationToken cancellationToken)
    {

        timer?.Change(Timeout.Infinite, 0);

        return Task.CompletedTask;
    }


    public async Task Publish()
    {

        try
        {
            timer = new System.Threading.Timer(
                _ => PublishJobs(),
                null,
                (int)TimeSpan.Zero.TotalMilliseconds,
                (int)TimeSpan.FromHours(5).TotalMilliseconds
            );
        }

        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }

    }


    private async Task PublishJobs()
    {

        try
        {
            bool hasPublished = await historyService.HasPublished();

            Console.WriteLine("to publish jobs to page...");

            // if (hasPublished)
            // {
            //     return;
            // }

            int waitCount = 1;
            while (hostedServiceManager.PublishingBlocked)
            {
                Console.WriteLine("Publishing to FB blocked waiting fetch... " + waitCount);
                if (waitCount > 10)
                {
                    Console.WriteLine("Waiting time exceeded aborting... ");
                    return;
                }
                System.Threading.Thread.Sleep(60000);
                waitCount++;
            }

            Job[] jobs = (await jobService.GetHottestAsync(8)).ToArray();

            byte[][] imagesBytes = await RenderService.GetRenderedJobsAsImages(jobs);

            FBImagePayload[] fbImagePayloads = FBImagePayload.FromJobsAndImagesBuilder(jobs, imagesBytes);

            String jobsHelloMessage = RenderService.RenderJobsHelloMessage();

            Console.WriteLine("Publishing to page...");

            String[] imagesIds = await fbPublisher.uploadImages(fbImagePayloads);

            await fbPublisher.PublishFeed(new FBMessagePayload()
            {
                Message = jobsHelloMessage,
                attached_media = imagesIds.Select<String, Object>(x => new { media_fbid = x }).ToArray()
            });

            //  await fbPublisher.PublishJobs(images.Take(2).ToArray());
            Console.WriteLine("Published XD");

            DateTime now = DateTime.Now;

            Console.WriteLine("saving FB publish history at {0} ", now.ToString("MMMM dd, yyyy"));

            await historyService.InsertOneAsync(
                new History()
                {
                    CreatedAt = now,
                    Type = HistoryTypes.FBPublish
                }
            );
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

    }

}