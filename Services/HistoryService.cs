using Microsoft.Extensions.Options;
using MongoDB.Driver;



using Models;

namespace Services;




public class HistoryService
{

    private readonly IMongoCollection<History> historyCollection;

    public DBService dbService { get; set; }

    public HistoryService(DBService dbService)
    {

        this.dbService = dbService;
        this.historyCollection = dbService.GetDB().GetCollection<History>("History");
    }

    public async Task<bool> HasScrappedTodayAtLeast()
    {

        var builder = Builders<History>.Filter;
        var filter = builder.Gt("createdAt", DateTime.Today.AddDays(-1).AddHours(5)) & builder.Eq("type", HistoryTypes.JobsScrap);

        long count = await historyCollection.CountDocumentsAsync(filter);

        Console.WriteLine("check can scrap count {0}", count);

        return count > 0;
    }


    public async Task<bool> HasPublished()
    {

        var builder = Builders<History>.Filter;
        var filter = builder.Gt("createdAt", DateTime.Now.AddHours(-2)) & builder.Eq("type", HistoryTypes.FBPublish);

        long count = await historyCollection.CountDocumentsAsync(filter);

        Console.WriteLine("check can publish count {0}", count);

        return count > 0;
    }

    public async Task InsertManyAsync(History[] histories) =>
        await historyCollection.InsertManyAsync(histories);

    public async Task DeleteManyAsync() =>
        await historyCollection.DeleteManyAsync(_ => true);


    public async Task<List<History>> GetAsync() =>
       await historyCollection.Find(_ => true).ToListAsync();


    public async Task<long> CountAsync() =>
        await historyCollection.CountDocumentsAsync(_ => true);

    public async Task<History> GetAsync(string id) =>
        await historyCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task InsertOneAsync(History newHistory) =>
        await historyCollection.InsertOneAsync(newHistory);


    public async Task UpdateAsync(string id, History updatedHistory) =>
        await historyCollection.ReplaceOneAsync(x => x.Id == id, updatedHistory);

    public async Task RemoveAsync(string id) =>
        await historyCollection.DeleteOneAsync(x => x.Id == id);


}