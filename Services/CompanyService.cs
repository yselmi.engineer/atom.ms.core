using Microsoft.Extensions.Options;
using MongoDB.Driver;



using Models;

namespace Services;




public class CompanyService
{

    private readonly IMongoCollection<Company> companiesCollection;

    private readonly IMongoCollection<Backup<Company>> companiesBackupCollection;

    public DBService dbService { get; set; }

    public CompanyService(DBService dbService)
    {

        this.dbService = dbService;
        this.companiesCollection = dbService.GetDB().GetCollection<Company>("Company");
        this.companiesBackupCollection = dbService.GetDB().GetCollection<Backup<Company>>("CompanyBackup");
    }



    public async Task InsertManyAsync(Company[] companies, bool backup = false)
    {


        await companiesCollection.InsertManyAsync(companies);

        if (backup)
        {
            DateTime createdAt = DateTime.Now;

            await companiesBackupCollection.InsertManyAsync(companies.Select(company => new Backup<Company>()
            {
                CreatedAt = createdAt,
                Data = company
            }).ToArray());
        }
    }



    public async Task DeleteManyAsync() =>
        await companiesCollection.DeleteManyAsync(_ => true);


    public async Task<List<Company>> GetAsync() =>
       await companiesCollection.Find(_ => true).ToListAsync();


    public async Task<long> CountAsync() =>
        await companiesCollection.CountDocumentsAsync(_ => true);

    public async Task<Company> GetAsync(string id) =>
        await companiesCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task InsertOneAsync(Company newCompany) =>
        await companiesCollection.InsertOneAsync(newCompany);


    public async Task UpdateAsync(string id, Company updatedCompany) =>
        await companiesCollection.ReplaceOneAsync(x => x.Id == id, updatedCompany);

    public async Task RemoveAsync(string id) =>
        await companiesCollection.DeleteOneAsync(x => x.Id == id);


}