using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Dynamic;
using Microsoft.Extensions.Options;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Net.Http;

using Settings;
using Models.FB;


namespace Services;



class ImageUploadResponse
{
    public String Id { get; set; }
}

public class FBPublisher
{

    private String Token { get; set; }
    private String PageURL { get; set; }

    private String PageFeedURL { get; set; }

    private String PageFeedPhotosURL { get; set; }


    private JsonSerializerOptions jsonSerializerOptions = new JsonSerializerOptions
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
    };

    private HttpClient client = new HttpClient();

    private String TokenizeURL(String url)
    {

        return $"{url}&access_token={Token}";
    }


    public FBPublisher(IOptions<FBSettings> fbSettings)
    {

        Token = fbSettings.Value.Token;
        PageURL = fbSettings.Value.PageURL;
        PageFeedURL = $"{fbSettings.Value.PageURL}/feed";
        PageFeedPhotosURL = $"{fbSettings.Value.PageURL}/photos";

        SetUrlsAsFormatted();
    }


    private void SetUrlsAsFormatted()
    {

        PageFeedURL = $"{PageFeedURL}?formatting=MARKDOWN";
        PageFeedPhotosURL = $"{PageFeedPhotosURL}?";
    }


    public async Task PublishFeed(FBMessagePayload payload)
    {

        Console.WriteLine("Publishing new feed to FB");
        System.Console.WriteLine(JsonSerializer.Serialize(payload, jsonSerializerOptions));

        HttpContent httpContent = new StringContent(
           JsonSerializer.Serialize(payload, jsonSerializerOptions),
            System.Text.Encoding.UTF8,
            "application/json"
            );

        HttpResponseMessage response = await client.PostAsync(TokenizeURL(PageFeedURL), httpContent);

        response.EnsureSuccessStatusCode();

        string responseBody = await response.Content.ReadAsStringAsync();

        Console.WriteLine("Published new feed to FB");
        Console.WriteLine(responseBody);

    }

    public async Task<String[]> uploadImages(FBImagePayload[] payloads)
    {

        System.Console.WriteLine("Uploading images to fb...");

        Console.WriteLine(payloads.Length);


        List<String> imagesIds = new List<String>();

        foreach (FBImagePayload payload in payloads)
        {


            HttpClient httpClient = new HttpClient();
            MultipartFormDataContent form = new MultipartFormDataContent();

            form.Add(new ByteArrayContent(payload.ImageBytes, 0, payload.ImageBytes.Length), "source", $"job{payload.ImageBytes.Length}.jpg");
            form.Add(new StringContent(payload.Message), "message");

            HttpResponseMessage response = await httpClient.PostAsync($"{TokenizeURL(PageFeedPhotosURL)}&published=false", form);

            string responseBody = await response.Content.ReadAsStringAsync();

            Console.WriteLine(responseBody);
            response.EnsureSuccessStatusCode();

            ImageUploadResponse imageUploadResponse = JsonSerializer.Deserialize<ImageUploadResponse>(responseBody, jsonSerializerOptions = new JsonSerializerOptions()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
            });

            imagesIds.Add(
                imageUploadResponse.Id
            );

            System.Threading.Thread.Sleep(1000);
        }

        return imagesIds.ToArray();

    }

    public async Task PublishPhoto(byte[] bytes = null)
    {

        System.Console.WriteLine("publishing fb photo");

        Console.WriteLine(bytes.Length);

        HttpClient httpClient = new HttpClient();
        MultipartFormDataContent form = new MultipartFormDataContent();


        form.Add(new ByteArrayContent(bytes, 0, bytes.Length), "source", "blockchain1.png");

        HttpResponseMessage response = await httpClient.PostAsync(TokenizeURL(PageFeedPhotosURL), form);

        response.EnsureSuccessStatusCode();

        string responseBody = await response.Content.ReadAsStringAsync();

        Console.WriteLine(responseBody);

    }

    private byte[] ImageToByteArray(Image imageIn)
    {
        using (var ms = new MemoryStream())
        {
            imageIn.Save(ms, imageIn.RawFormat);
            return ms.ToArray();
        }
    }

}

