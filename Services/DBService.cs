using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Settings;

using Models;

namespace Services;




public class DBService
{


    public IOptions<DBSettings> DBSettings { get; set; }

    public DBService(IOptions<DBSettings> dbSettings)
    {
        DBSettings = dbSettings;
    }


    public IMongoDatabase GetDB()
    {

        var mongoClient = new MongoClient(
          DBSettings.Value.ConnectionString);

        return mongoClient.GetDatabase(
            DBSettings.Value.DatabaseName);
    }


}