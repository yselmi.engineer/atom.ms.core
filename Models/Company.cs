using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace Models;



public class CompanyDTO
{

    public String Name { get; set; }

    public String Slug { get; set; }

    public String Logo { get; set; }
}
public class Company
{


    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public String? Id { get; set; }

    public String Name { get; set; }

    public String Slug { get; set; }

    public String Logo { get; set; }


    public Boolean HasUsedSponsoring { get; set; }



    public Company SetHasUsedSponsoring(Boolean value)
    {
        HasUsedSponsoring = value;
        return this;
    }

}