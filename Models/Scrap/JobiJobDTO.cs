using System;
using System.Collections.Generic;

using System.Text.Json.Serialization;


namespace Models.Scrap;





public class JobiJobsDtO
{

    public JobiJobDtO[]? Hits { get; set; }
}


public class JobiJobDtO
{

    public Boolean? Active { get; set; }

    [JsonPropertyName("all_tags")]
    public String[]? AllTags { get; set; }

    public String[]? Tags { get; set; }

    public String? Title { get; set; }

    public String? Benefits { get; set; }

    public String? Description { get; set; }

    public CompanyDTO? Company { get; set; }
    public Dictionary<string, Object?>? Degree { get; set; }

    public Dictionary<string, Object?>? Experience { get; set; }


    [JsonPropertyName("job_type")]
    public Dictionary<string, Object?>? Contract { get; set; }


    [JsonPropertyName("salary_max")]
    public int? SalaryMax { get; set; }

    [JsonPropertyName("salary_min")]
    public int? SalaryMin { get; set; }


    [JsonPropertyName("created_at")]
    public DateTime? CreatedAt { get; set; }


    [JsonPropertyName("suspend_date")]
    public DateTime? Deadline { get; set; }


    public Job Transform()
    {
        return new Job()
        {
            Active = Active,
            AllTags = AllTags,
            Tags = Tags,
            Title = Title,
            Benefits = Benefits,
            Description = Description,
            Company = Company,
            Degree = Degree?.GetValueOrDefault("name")?.ToString(),
            Experience = Experience?.GetValueOrDefault("value")?.ToString(),
            Contract = Contract?.GetValueOrDefault("name")?.ToString(),
            SalaryMax = SalaryMax,
            SalaryMin = SalaryMin,
            CreatedAt = CreatedAt,
            Deadline = Deadline

        };
    }
}