



namespace Models.Scrap;


public class JobiSponsoredCompanyDTO : ICompanyTransformable
{

    public String? Name { get; set; }


    public String? Slug { get; set; }

    public String? Logo { get; set; }


    public Company Transform()
    {


        return new Company()
        {
            Name = Name,
            Slug = Slug,
            Logo = Logo

        };
    }

}