namespace Models.Payload;



public class JobiScrapPayload
{


    public String[] AttributesToSearch { get; } = new String[] {
    "title",
    "company.name",
    "tags",
    "summary",
    "description",
    "location.name"
    };

    public int Domain_company { get; } = 1;

    public Facet[] Facets { get; } = new Facet[] {
       new Facet("salary_min", "min"),
       new Facet("salary_max", "max"),
       new Facet("job_type.name.full", "terms"),
       new Facet("company.name.full", "terms"),
       new Facet("category.name.full", "terms"),
       new Facet("location.name.full", "terms", 0),
       new Facet("tags.full", "terms")
    };

    public int HitsPerPage { get; set; } = 10;
    public int Page { get; set; } = 1;

    public String Parser { get; } = "source";
    public String SortBy { get; } = "updated_at";
    public String SortOrder { get; } = "desc";



    public void SetNextPagination() {
        Page++;
    }


}


public class Facet
{

    public String Field { get; set; }

    public String Type { get; set; }


    public Int32? Size { get; set; } = null;


    public Facet(String field, String type, Int32 size)
    {
        Field = field;
        Type = type;
        Size = size;
    }

    public Facet(String field, String type)
    {
        Field = field;
        Type = type;
    }

}