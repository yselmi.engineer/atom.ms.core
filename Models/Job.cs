using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace Models;




public class Job
{


    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public String? Id { get; set; }
    public Boolean? Active { get; set; }


    public String[]? AllTags { get; set; }

    public String[]? Tags { get; set; }

    public String? Title { get; set; }

    public String? Benefits { get; set; }

    public String? Description { get; set; }
    public CompanyDTO? Company { get; set; }

    public String? Degree { get; set; }

    public String? Experience { get; set; }

    public String? Contract { get; set; }

    private int? salaryMax;
    public int? SalaryMax
    {
        get { return salaryMax; }
        set
        {
            if (value.ToString().Length > 4)
            {
                salaryMax = Int32.Parse(value.ToString().Remove(value.ToString().Length - 1, 1));
                return;
            }

            salaryMax = value;
        }
    }

    private int? salaryMin;
    public int? SalaryMin
    {
        get { return salaryMin; }
        set
        {
            if (value.ToString().Length > 4)
            {
                salaryMin = Int32.Parse(value.ToString().Remove(value.ToString().Length - 1, 1));
                return;
            }

            salaryMin = value;
        }
    }
    public DateTime? CreatedAt { get; set; }

    public DateTime? Deadline { get; set; }


    public Dictionary<String, String> RenderAsText()
    {
        return new Dictionary<String, String>() {
            {"Title", Title},
            {"Company", Company.Name},
            {"Status", Active == true ? "Offer Still available": "Offer Expired"},
            {"MinSalary", SalaryMin.ToString()},
            {"MaxSalary", SalaryMax.ToString()},
            {"Description", Description},
            {"Benefits", Benefits},
            {"PublishedAt", CreatedAt?.ToString("MMMM dd, yyyy")},
            {"ExpiresAt", Deadline?.ToString("MMMM dd, yyyy")},
            { "Tags", String.Join(", ", Tags.Select(t => t.Contains('#') ? t : $"#{t}").Select(t => t.Replace("#.", string.Empty).Replace("-", string.Empty)))}

        };
    }
}
