using System;
using Services;

namespace Models.FB;




public class FBImagePayload
{
    public String Message { get; set; }


    public byte[] ImageBytes { get; set; }



    public static FBImagePayload[] FromJobsAndImagesBuilder(Job[] jobs, byte[][] imagesBytes)
    {


        if (jobs.Length != imagesBytes.Length) throw new ArgumentException("invalid array sizes");


        return jobs.Select((it, index) => new FBImagePayload()
        {
            Message = RenderService.RenderJobAsText(it),
            ImageBytes = imagesBytes[index]
        }).ToArray();

    }

}

