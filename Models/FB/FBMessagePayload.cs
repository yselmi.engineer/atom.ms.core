namespace Models.FB;



public static class FBMessagePayloadPrivayOptions
{
    public static String Self = "SELF";
}



public class FBMessagePayload
{
    public String Message { get; set; }


    public String Formatting { get; set; } = "MARKDOWN";


    public dynamic[] attached_media { get; set; }


}