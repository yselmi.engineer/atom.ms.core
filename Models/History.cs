
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace Models;





public static class HistoryTypes
{

    public static String JobsScrap = "JOBS_SCRAP";
    public static String FBPublish = "FB_PUBLISH";
}
public class History
{


    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public String? Id { get; set; }


    public DateTime? CreatedAt { get; set; }


    public String? Type { get; set; }


}