using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Models;


public class Backup<T>
{

    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public String? Id { get; set; }

    public DateTime? CreatedAt { get; set; }

    public T? Data { get; set; }
}