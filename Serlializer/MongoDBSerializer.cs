using System;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization.Conventions;


namespace Serializers;



public class LowerCamelCaseSerializer : ConventionBase, IMemberMapConvention
{
    public void Apply(BsonMemberMap memberMap)
    {
        string name = memberMap.MemberName;
        name = char.ToLower(name[0]) + name.Substring(1);
        memberMap.SetElementName(name);
    }
}