namespace Settings;

public class FBSettings
{
    public string Token { get; set; } = null!;

    public string PageURL { get; set; } = null!;

}