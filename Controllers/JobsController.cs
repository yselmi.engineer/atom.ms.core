using System;
using Microsoft.AspNetCore.Mvc;
using Services;
using Models;

namespace Core.Controllers;

[ApiController]
[Route("/api/jobs")]
public class JobsController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<JobsController> _logger;

    private readonly IScrapper scrapper;
    public JobsController(ILogger<JobsController> logger, IScrapper ScrapperHostedService)
    {
        // ScrapperHostedService.Scrap();
        _logger = logger;
        _logger.LogInformation("jobs ctrl called");

    }

    [HttpGet()]
    public List<Job> GetJobs()
    {

        return JobiScrapper.JobiJobs;
    }


}
