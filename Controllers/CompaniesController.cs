using System;
using Microsoft.AspNetCore.Mvc;
using Services;
using Models;

namespace Core.Controllers;

[ApiController]
[Route("/api/companies")]
public class CompaniesController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<CompaniesController> _logger;
    public CompaniesController(ILogger<CompaniesController> logger)
    {
        _logger = logger;
        _logger.LogInformation("companies ctrl called");

    }

    [HttpGet]
    public IEnumerable<Company> GetCompanies()
    {
        return new List<Company>();
    }


}
