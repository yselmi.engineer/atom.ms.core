using Settings;
using Services;
using Serializers;
using MongoDB.Bson.Serialization.Conventions;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



builder.Services.Configure<FBSettings>(
    builder.Configuration.GetSection("FB"));

builder.Services.Configure<DBSettings>(
    builder.Configuration.GetSection("JobsDB"));


builder.Services.AddSingleton<FBPublisher>();
builder.Services.AddSingleton<DBService>();
builder.Services.AddSingleton<CompanyService>();
builder.Services.AddSingleton<JobService>();
builder.Services.AddSingleton<HistoryService>();
builder.Services.AddSingleton<HostedServiceManager>();

builder.Services.AddHostedService<FBHostedService>();
builder.Services.AddHostedService<ScrapperHostedService>();


var pack = new ConventionPack();
pack.Add(new LowerCamelCaseSerializer());

ConventionRegistry.Register(
    "LowerCamelCaseConvention",
    pack,
    t => true);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
